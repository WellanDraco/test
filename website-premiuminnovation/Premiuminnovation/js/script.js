const menubouton = document.querySelector('a#toggle');
const menu = document.querySelector('header');

function togglemenu() {
	'use strict';
	menu.classList.toggle('on');
	menubouton.classList.toggle('on');
}

const active = false; // code brouillon desactivé pour la video 360

if (menu.offsetWidth >= 600 && active == true) {
	const url = 'https://aframe.io/releases/0.8.0/aframe.min.js';
	// adding the script tag to the head
	let head = document.getElementsByTagName('head')[0];
	const script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = url;

	// fire the loading
	head.appendChild(script);

	const intro = document.getElementById('intro');
	const scene = document.createElement('a-scene');
	intro.appendChild(scene);

	const assets = document.createElement('a-assets');
	scene.appendChild(assets);

	const video = document.createElement('video');
	video.setAttribute('id', video);
	video.autoplay = true;
	video.loop = true;
	video.src = 'https://test.arthur-moug.in/website/styles/video/CETTEVIDEO.mp4';
	assets.appendChild(video);

	const sphere = document.createElement('a-videosphere');
	sphere.src = '#video';
	scene.appendChild(sphere);
}

const lesH2Sphere = document.querySelectorAll('#sphere>ul>li');
const lesPActiCli = document.querySelectorAll('#actcli>#activite>ul>li,#actcli>#client>ul>li');
let listEl = new Array();
for (let i = 0; i < lesH2Sphere.length; i++) {
	const elsClassIdentique = new Array();
	elsClassIdentique.push(lesH2Sphere[i]);
	elsClassIdentique.push(lesPActiCli[i]);
	listEl.push(elsClassIdentique);
}

function ApplyDisplay() {
	for (el of listEl) {
		if (el[0].classList[2]) {
			el[1].classList.add('actif');
			//console.log(el[1]);
		} else {
			el[1].classList.remove('actif');
			//console.log(el[1]);
		}
	}
}
ApplyDisplay();

function changdisplay() {
	var previous = document.querySelector('.' + this.classList[1] + '.actif');
	if (previous != this) {
		previous.classList.remove('actif');
		this.classList.add('actif');
		ApplyDisplay();
	}
}

for (el of listEl) {
	el[0].addEventListener('click', changdisplay);
}

//code pour le placement des items


function draw() {	

	const lalist = document.querySelectorAll('#sphere > ul > li'); //liste des éléments a tourner

	let width = document.body.offsetWidth; //100vw
	
	if (width >= 1250){ //limit max
		width= 1700;
	} else if(width <= 430) {
		width=430;
	}

	let circlewidth = 0.3 * width + 100; //30vw + correctif
	let correctifX = 120;
	let correctifY = 92;

	if (width < 900) {
		circlewidth = 0.5 * width - 35
		correctifX = 0.12 *width;
		correctifY = 0.11 * width;
	}

	let angleFactor = 215 / circlewidth; // on cherche a obtenir l'angle permettant de laisser 215px d'espace a chaque item autour du cercle, angleFactor est l'angle entre 2 items

	if (angleFactor <= 0.3) { //valeur minimale de l'angle
		angleFactor = 0.3;
	} else if (angleFactor >= 0.58){ // valeur maximale
		angleFactor = 0.58;
	}



	for (let i = 0; i < lalist.length; i++) {
		let el = lalist[i];
		let angle = -9.3 + angleFactor * i; //-9.5 est l'angle de départ
		el.style.transform =
			'translate(calc(' +
			circlewidth * Math.sin(angle) +
			'px - '+correctifX+'px),calc(' +
			circlewidth * Math.cos(angle) +
			'px  - '+correctifY+'px))'; //-120px et -98 px sont des corrections pour centrer approchimativement le cercle
	}
};
draw();
window.addEventListener('resize', draw)

const target = document.querySelector('#sphere > div');

target.addEventListener('click', function() {
	//document.location.href = 'http://localhost/testserv/vr'; // a changer
})


var someDiv = document.getElementById('actcli');
var firstval = someDiv.getBoundingClientRect().top;



