AFRAME.registerComponent('animation-module', {
    schema: {
        masselots: {default: []},
        arrows: {default: []}
    },
    init: function () {

    },
    update: function () {},
    tick: function () {
        return 0;
        if (this.data.masselots.length === 0) return 0;
        //console.log(this.data.masselots[0].position.x);
        this.data.arrows[0].object3D.position.set(this.data.masselots[0].position);
    },
    remove: function () {},
    pause: function () {},
    play: function () {
        var obj = this.el.object3D;
        var modlist = obj.children[0].children;
        for(let i = 0; i < modlist.length;i++){
            if(modlist[i].name.startsWith("Masselotte_")){
                this.data.masselots.push(modlist[i]);
            }
        }
        console.log(this.data.masselots);

        this.data.arrows = document.querySelectorAll("#arrows *");
        console.log(this.data.arrows);

    }
});
