console.log("hi");


let mycanvas = document.querySelector(".a-canvas");
let myheight = mycanvas.height;
let count = 0;
let scene = document.querySelector('a-scene');
let eyeButton = document.getElementById("eye");
let PlayPauseButton = document.getElementById("playpause");
let playing = true;
let model = document.querySelector('a-gltf-model');
let overlay = document.getElementById("overlay");
scene.addEventListener("renderstart",sceneStarted);

function sceneStarted(e){
   checkMedia();
}

async function checkMedia(){
    if(myheight  !== mycanvas.height){
        count++;
        myheight = mycanvas.height;
    }
    if( count < 3 )  {
        setTimeout(checkMedia,500);
        return 0;
    }
    showUI();
}

function showUI(){
    console.log("Shown");
    let hiddens = document.querySelectorAll(".hidden");
    hiddens[0].classList.remove("hidden");
    hiddens[1].classList.remove("hidden");
}

function toogleView() {
    console.log(eyeButton);
    eyeButton.classList.toggle("hiding");
    eyeButton.classList.toggle("viewing");
    overlay.classList.toggle("closed");
};

function tooglePlayPause() {
    console.log(PlayPauseButton);
    PlayPauseButton.classList.toggle("playing");
    PlayPauseButton.classList.toggle("pausing");
    playing = !playing;
    if(playing){
        model.setAttribute('animation-mixer', {timeScale: 1});
    }
    else {
        model.setAttribute('animation-mixer', {timeScale: 0});
    }
};

