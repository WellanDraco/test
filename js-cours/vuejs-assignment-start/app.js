Vue.component('app-liste', {
    props: ['hobbies'],
    data: function() {
        return {
            //hobbies: ["dance", "design", "vr dev"]
        };
    },
    template: '<ul><li v-on:click="remove(el)" v-for="el in hobbies">{{el}}</li></ul>',
    methods: {
        addHobbie: function() {
            this.hobbies.push(this.NewHobby);
            this.lastAct = 1;
        },
        remove: function(el) {
            this.hobbies.splice(this.hobbies.indexOf(el), 1);
            this.lastAct = -1;
        },
        getColor: function(count) {
            return count < 4 ? 'white' : 'blue';
        }
    }
})


new Vue({
    el: '#vueapp',
    data: {
        hobbies: ["dance", "design", "vr dev"],
        NewHobby: "",
        lastAct: 1,
    },
    methods: {
        addHobbie: function() {
            this.hobbies.push(this.NewHobby);
            this.lastAct = 1;
        },
        remove: function(el) {
            this.hobbies.splice(this.hobbies.indexOf(el), 1);
            this.lastAct = -1;
        },
        getColor: function(count) {
            return count < 4 ? 'white' : 'blue';
        }
    }
});