AFRAME.registerComponent('cursor-listener', {
    init: function () {
        var el = this.el;
        var lastUpdate = Date.now();
        el.addEventListener('mousedown', openClose);




        function openClose(evt) {
            console.log("az");
            if((Date.now() -lastUpdate) > 2000){
                var array = el.children;
                if(!el.classList.contains("vertical"))
                {
                    el.emit("vertical",true);
                    for(var e = 0; e < array.length;e++){
                        // console.log(el.children[e]);
                        el.children[e].emit("ouverture");
                    }

                }
                else {
                    el.emit("horizontal",true);
                    for(var e = 0; e < array.length;e++){
                        // console.log(el.children[e]);
                        el.children[e].emit("fermeture");
                    }
                }
                el.classList.toggle("vertical");
                lastUpdate = Date.now()
            }
        }
    }


});
